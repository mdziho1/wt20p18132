DELETE,/v1/all,null,{\"message\":\"Uspješno obrisan sadržaj datoteka!\"}
GET,/v1/predmeti,null,[]
POST,/v1/predmet,{\"naziv\":\"RMA\"},{\"message\":\"Uspješno dodan predmet!\"}
POST,/v1/predmet,{\"naziv\":\"WT\"},{\"message\":\"Uspješno dodan predmet!\"}
GET,/v1/predmeti,null,[{\"naziv\":\"RMA\"},{\"naziv\":\"WT\"}]
GET,/v1/aktivnosti,null,[]
POST,/v1/aktivnost,{\"naziv\":\"WT\",\"tip\":\"Predavanje\",\"pocetak\":\"9\", \"kraj\":\"12\", \"dan\":\"Utorak\"},{\"message\":\"Uspješno dodana aktivnost!\"}
POST,/v1/aktivnost,{\"naziv\":\"OOI\",\"tip\":\"Vjezbe\",\"pocetak\":\"10\", \"kraj\":\"11\", \"dan\":\"Srijeda\"},{\"message\":\"Uspješno dodana aktivnost!\"}
POST,/v1/aktivnost,{\"naziv\":\"WT\",\"tip\":\"Vjezbe\",\"pocetak\":\"10\", \"kraj\":\"11\", \"dan\":\"Srijeda\"},{\"message\":\"Aktivnost nije validna!\"}
GET,/v1/aktivnosti,null,[{\"naziv\":\"WT\",\"tip\":\"Predavanje\",\"pocetak\": 9, \"kraj\":12, \"dan\":\"Utorak\"}, {\"naziv\":\"OOI\",\"tip\":\"Vjezbe\",\"pocetak\":10, \"kraj\":11, \"dan\":\"Srijeda\"}]
DELETE,/v1/aktivnost/NE_POSTOJI,null,{\"message\":\"Greška - aktivnost nije obrisana!\"}
DELETE,/v1/aktivnost/WT,null,{\"message\":\"Uspješno obrisana aktivnost!"}
GET,/v1/predmet/WT/aktivnost,null,[]
GET,/v1/predmet/OOI/aktivnost,null,[{\"naziv\":\"OOI\",\"tip\":\"Vjezbe\",\"pocetak\":10, \"kraj\":11, \"dan\":\"Srijeda\"}]
GET,/v1/aktivnosti,null,[{\"naziv\":\"OOI\",\"tip\":\"Vjezbe\",\"pocetak\":10, \"kraj\":11, \"dan\":\"Srijeda\"}]
DELETE,/v1/predmet/NE_POSTOJI,null,{\"message\":\"Greška - predmet nije obrisan!\"}
DELETE,/v1/predmet/RMA,null,{\"message\":\"Uspješno obrisan predmet!\"}
GET,/v1/predmeti,null,[{\"naziv\":\"WT\"}]
DELETE,/v1/all,null,{\"message\":\"Uspješno obrisan sadržaj datoteka!\"}
