const express = require('express');
const bodyParser = require('body-parser')

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'))

// sequelize instanca
const sequelize = require("./models/sequelize");

require('./models/models')(sequelize);

sequelize.sync({ alter: true }).then(() => {
    console.log("Svi modeli su sinhrovani sa bazom.");
})

// v1 rute
require('./v1routes')(app);

// v2 rute
require('./v2routes')(app);

if (require.main === module) {      // Dodao ovu liniju zbog pokretanja testova
    app.listen(3000);
}

module.exports = app;


