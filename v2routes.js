module.exports = function (app) {

    require('./controllers/predmet')(app);
    require('./controllers/student')(app);
    require('./controllers/grupa')(app);
    require('./controllers/aktivnost')(app);
    require('./controllers/dan')(app);
    require('./controllers/tip')(app);
}