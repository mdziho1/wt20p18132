const sequelize = require('../models/sequelize')

const { Predmet } = sequelize.models;

module.exports = function (app) {

    app.get('/v2/predmet', async (req, res) => {
        const predmeti = await Predmet.findAll();
        res.json(predmeti);
    });

    app.post('/v2/predmet', async (req, res) => {
        const predmet = await Predmet.create(req.body).catch(() => {
            res.status(400)
            return res.json({ message: "Naziv predmeta postoji!" })
        });

        res.json(predmet)
    });

    app.put('/v2/predmet/:id', async (req, res) => {
        const id = req.params.id;

        return Predmet.findOne({ where: { id: id } }).then((predmet) => {
            if (predmet) {
                predmet.update(req.body).then(p => {
                    return res.json(p);
                })
            } else {
                return res.json({ message: "Predmet nije nađen." })
            }

        });
    });

    app.delete('/v2/predmet/:id', async (req, res) => {
        const id = req.params.id;

        return Predmet.findOne({ where: { id: id } }).then((predmet) => {
            if (predmet) {
                predmet.destroy().then(() => {
                    return res.json({ message: "Predmet izbrisan." })
                })
            } else {
                return res.json({ message: "Predmet nije nađen." })
            }
        });
    });
}