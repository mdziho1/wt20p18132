const sequelize = require('../models/sequelize')

const { Student, Grupa } = sequelize.models;

module.exports = function (app) {
    app.get('/v2/student', async (req, res) => {
        const studenti = await Student.findAll({ include: Grupa });
        res.json(studenti);
    });

    app.post('/v2/student', async (req, res) => {
        const student = await Student.create(req.body);

        res.json(student)
    });

    app.put('/v2/student/:id', async (req, res) => {
        const id = req.params.id;

        return Student.findOne({ where: { id: id } }).then((student) => {
            if (student) {
                student.update(req.body).then(p => {
                    return res.json(p);
                })
            } else {
                return res.json({ message: "Student nije nađen." })
            }

        });
    });

    app.delete('/v2/student/:id', async (req, res) => {
        const id = req.params.id;

        return Student.findOne({ where: { id: id } }).then((student) => {
            if (student) {
                student.destroy().then(() => {
                    return res.json({ message: "Student izbrisan." })
                })
            } else {
                return res.json({ message: "Student nije nađen." })
            }
        });
    });

    app.post("/v2/dodajStudente", async (req, res) => {
        const { studenti } = req.body;

        let GrupaId = Number(req.body.grupaId);

        const grupa = await Grupa.findOne({ where: { id: GrupaId } })

        const kreiraniStudenti = [];
        const neuspjesnePoruke = [];

        const promises = [];

        // Kreacija svakog studenta je asinhrona, tj. Student.create ce vratiti promise i samo cemo ga gurnuti u promises niz

        studenti.forEach(student => {
            promises.push(Student.create({ GrupaId: GrupaId, ...student })
                .then(noviStudent => {
                    return noviStudent.addGrupa(grupa).then(() => kreiraniStudenti.push(noviStudent))
                })
                .catch(ex => {

                    // Postoji student sa poslanim indeksom
                    return Student.findOne({ where: { index: student.index } })
                        .then(async postojeci => {

                            if (postojeci.ime === student.ime) {         // Postoji identican student, dakle treba mu ili dodati ili izmjenuti grupu
                                return await dodajIliIzmijeniGrupu(postojeci, grupa);
                            }

                            return neuspjesnePoruke.push(`Student ${student.ime} nije kreiran jer postoji student ${postojeci.ime} sa istim indexom ${postojeci.index}`)
                        })
                })
            );
        })

        // Promise.all ce sacekati sve promises da se zavrse, pa onda mozemo uraditi .then i vratiti konacni response
        return Promise.all(promises).then(() => {

            if (neuspjesnePoruke.length === 0) {       // nije bilo errora
                res.status(200);
            }

            return res.json({
                kreirani: kreiraniStudenti,
                poruke: neuspjesnePoruke
            });
        })
    });

    async function dodajIliIzmijeniGrupu(student, novaGrupa) {
        const studentoveGrupe = await student.getGrupe();

        for (let i = 0; i < studentoveGrupe.length; i++) {
            if (studentoveGrupe[i].PredmetId === novaGrupa.PredmetId) {       // student je vec upisan u grupu za isti predmet, treba zamijeniti grupe
                await student.removeGrupa(studentoveGrupe[i])
                await student.addGrupa(novaGrupa);
                return;
            }
        }

        return student.addGrupa(novaGrupa);     // Ako nije bilo zamjene, samo dodati grupu
    }
}