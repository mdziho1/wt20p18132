const sequelize = require('../models/sequelize')

const { Grupa } = sequelize.models;

module.exports = function (app) {
    app.get('/v2/grupa', async (req, res) => {
        const grupe = await Grupa.findAll({ raw: true });
        res.json(grupe);
    });

    app.post('/v2/grupa', async (req, res) => {
        const grupa = await Grupa.create(req.body);

        res.json(grupa)
    });

    app.put('/v2/grupa/:id', async (req, res) => {
        const id = req.params.id;

        return Grupa.findOne({ where: { id: id } }).then((grupa) => {
            if (grupa) {
                grupa.update(req.body).then(p => {
                    return res.json(p);
                })
            } else {
                return res.json({ message: "Grupa nije nađena." })
            }

        });
    });

    app.delete('/v2/grupa/:id', async (req, res) => {
        const id = req.params.id;

        return Grupa.findOne({ where: { id: id } }).then((grupa) => {
            if (grupa) {
                grupa.destroy().then(() => {
                    return res.json({ message: "Grupa izbrisana." })
                })
            } else {
                return res.json({ message: "Grupa nije nađena." })
            }
        });
    });
}