const sequelize = require('../models/sequelize')

const { Dan } = sequelize.models;

module.exports = function (app) {
    app.get('/v2/dan', async (req, res) => {
        const dani = await Dan.findAll();
        res.json(dani);
    });

    app.post('/v2/dan', async (req, res) => {
        const dan = await Dan.create(req.body);

        res.json(dan)
    });

    app.put('/v2/dan/:id', async (req, res) => {
        const id = req.params.id;

        return Dan.findOne({ where: { id: id } }).then((dan) => {
            if (dan) {
                dan.update(req.body).then(p => {
                    return res.json(p);
                })
            } else {
                return res.json({ message: "Dan nije nađen." })
            }

        });
    });

    app.delete('/v2/dan/:id', async (req, res) => {
        const id = req.params.id;

        return Dan.findOne({ where: { id: id } }).then((dan) => {
            if (dan) {
                dan.destroy().then(() => {
                    return res.json({ message: "Dan izbrisan." })
                })
            } else {
                return res.json({ message: "Dan nije nađen." })
            }
        });
    });
}