const sequelize = require('../models/sequelize')

const { Aktivnost, Dan, Tip, Predmet } = sequelize.models;

module.exports = function (app) {
    app.get('/v2/aktivnost', async (req, res) => {
        const aktivnosti = await Aktivnost.findAll();
        res.json(aktivnosti);
    });

    app.post('/v2/aktivnost', async (req, res) => {

        let { naziv, tip, pocetak, kraj, dan } = req.body;

        pocetak = Number(pocetak);
        kraj = Number(kraj);

        const [danObj] = await Dan.findOrCreate({ where: { naziv: dan } });
        const [tipObj] = await Tip.findOrCreate({ where: { naziv: tip } })
        const [predmetObj] = await Predmet.findOrCreate({ where: { naziv: naziv } });

        const sveAktivnostiDana = await Aktivnost.findAll({ where: { DanId: danObj.id } })

        if (!validirajNovuAktivnost(sveAktivnostiDana, pocetak, kraj)) {
            res.status(400);
            return res.json({ message: "Aktivnost nije validna!" })
        }

        const akt = {
            naziv: naziv + " " + tip,
            pocetak: pocetak,
            kraj: kraj,
            PredmetId: predmetObj.id,
            DanId: danObj.id,
            TipId: tipObj.id,
        }

        const aktivnost = await Aktivnost.create(akt).catch(ex => {
            res.status(400);
            return res.json({ message: "Doslo je do greske kod kreiranja aktivnosti!" })
        });

        res.json(aktivnost)
    });

    app.put('/v2/aktivnost/:id', async (req, res) => {
        const id = req.params.id;

        return Aktivnost.findOne({ where: { id: id } }).then((aktivnost) => {
            if (aktivnost) {
                aktivnost.update(req.body).then(p => {
                    return res.json(p);
                })
            } else {
                return res.json({ message: "Aktivnost nije nađena." })
            }

        });
    });

    app.delete('/v2/aktivnost/:id', async (req, res) => {
        const id = req.params.id;

        return Aktivnost.findOne({ where: { id: id } }).then((aktivnost) => {
            if (aktivnost) {
                aktivnost.destroy().then(() => {
                    return res.json({ message: "Aktivnost izbrisana." })
                })
            } else {
                return res.json({ message: "Aktivnost nije nađena." })
            }
        });
    });

    function validirajNovuAktivnost(dnevneAktivnosti, pocetak, kraj) {

        if (pocetak > kraj) {
            return false;
        }

        let aktivnostValidna = true;

        if (dnevneAktivnosti) {
            for (let i = 0; i < dnevneAktivnosti.length; i++) {

                const punoPreklapanje = pocetak <= dnevneAktivnosti[i].pocetak && kraj >= dnevneAktivnosti[i].pocetak;

                const parcijalnoPreklapanje = pocetak >= dnevneAktivnosti[i].pocetak && pocetak < dnevneAktivnosti[i].kraj;

                if (punoPreklapanje || parcijalnoPreklapanje) {
                    aktivnostValidna = false;
                    break;
                }
            }
        }

        return aktivnostValidna;
    }
}