const sequelize = require('../models/sequelize')

const { Tip } = sequelize.models;

module.exports = function (app) {
    app.get('/v2/tip', async (req, res) => {
        const tipi = await Tip.findAll();
        res.json(tipi);
    });

    app.post('/v2/tip', async (req, res) => {
        const tip = await Tip.create(req.body);

        res.json(tip)
    });

    app.put('/v2/tip/:id', async (req, res) => {
        const id = req.params.id;

        return Tip.findOne({ where: { id: id } }).then((tip) => {
            if (tip) {
                tip.update(req.body).then(p => {
                    return res.json(p);
                })
            } else {
                return res.json({ message: "Tip nije nađen." })
            }

        });
    });

    app.delete('/v2/tip/:id', async (req, res) => {
        const id = req.params.id;

        return Tip.findOne({ where: { id: id } }).then((tip) => {
            if (tip) {
                tip.destroy().then(() => {
                    return res.json({ message: "Tip izbrisan." })
                })
            } else {
                return res.json({ message: "Tip nije nađen." })
            }
        });
    });
}