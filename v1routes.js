const fs = require('fs')

module.exports = function(app) {
    function dajSveAktivnosti() {

        const data = fs.readFileSync('aktivnosti.txt', 'utf-8')

        if (!data.length) {
            return [];
        }

        const split = data.split('\n').filter(a => Boolean(a));

        return split.map(a => {

            const podaciAktivnosti = a.split(',');

            if (!podaciAktivnosti) return null;

            return ({
                naziv: podaciAktivnosti[0],
                tip: podaciAktivnosti[1],
                pocetak: Number(podaciAktivnosti[2]),
                kraj: Number(podaciAktivnosti[3]),
                dan: podaciAktivnosti[4],
            })
        });
    }

    function dajSvePredmete() {
        const data = fs.readFileSync('predmeti.txt', 'utf-8')

        if (!data.length) {
            return [];
        }

        return data.split('\n').filter(p => Boolean(p));
    }

    function validirajNovuAktivnost(dnevneAktivnosti, pocetak, kraj) {

        if (pocetak > kraj) {
            return false;
        }

        let aktivnostValidna = true;

        if (dnevneAktivnosti) {
            for (let i = 0; i < dnevneAktivnosti.length; i++) {

                const punoPreklapanje = pocetak <= dnevneAktivnosti[i].pocetak && kraj >= dnevneAktivnosti[i].pocetak;

                const parcijalnoPreklapanje = pocetak >= dnevneAktivnosti[i].pocetak && pocetak < dnevneAktivnosti[i].kraj;

                if (punoPreklapanje || parcijalnoPreklapanje) {
                    aktivnostValidna = false;
                    break;
                }
            }
        }

        return aktivnostValidna;
    }

    app.get('/v1/predmeti', function (req, res) {
        fs.readFile('predmeti.txt', 'utf-8', (err, data) => {
            if (err) throw err;

            res.json(dajSvePredmete().map(p => ({ naziv: p })));
        });
    })

    app.get('/v1/aktivnosti', function (req, res) {
        fs.readFile('aktivnosti.txt', 'utf-8', (err, data) => {
            if (err) throw err;
            res.json(dajSveAktivnosti())
        });
    })

    app.get('/v1/predmet/:naziv/aktivnost', function (req, res) {

        const predmet = req.params.naziv;

        const sveAktivnosti = dajSveAktivnosti();

        res.json(sveAktivnosti.filter(a => a.naziv === predmet))
    })

    app.post('/v1/predmet', function (req, res) {
        const predmeti = dajSvePredmete();

        const noviPredmet = req.body.naziv;

        if (predmeti.includes(noviPredmet)) {
            res.status(400);
            return res.json({ message: "Naziv predmeta postoji!" })
        }

        const noviUnos = `${predmeti.length > 0 ? "\n" : ''}${noviPredmet}`        // Provjera za \n

        // Dodavanje predmeta
        fs.appendFile('predmeti.txt', noviUnos, (err) => {
            if (err) throw err;

            res.json({ message: "Uspješno dodan predmet!" })
        });
    })

    app.post('/v1/aktivnost', function (req, res) {
        let { naziv, tip, pocetak, kraj, dan } = req.body;

        pocetak = Number(pocetak)
        kraj = Number(kraj);

        const sveAktivnosti = dajSveAktivnosti()

        const dnevneAktivnosti = sveAktivnosti.filter(a => a.dan === dan);

        if (!validirajNovuAktivnost(dnevneAktivnosti, pocetak, kraj)) {
            res.status(400);
            return res.json({ message: "Aktivnost nije validna!" })
        }

        const novaAktivnost = `${sveAktivnosti.length > 0 ? "\n" : ''}${naziv},${tip},${pocetak},${kraj},${dan}`

        fs.appendFile('aktivnosti.txt', novaAktivnost, (err) => {
            if (err) throw err;

            res.json({ message: "Uspješno dodana aktivnost!" })
        });
    })

    app.delete('/v1/aktivnost/:naziv', function (req, res) {

        const aktivnost = req.params.naziv;

        const sveAktivnosti = dajSveAktivnosti();

        const filtrirano = sveAktivnosti.filter(a => a.naziv !== aktivnost);

        // Ako nista ne treba izbrisati
        if (sveAktivnosti.length === filtrirano.length) {
            return res.json({ message: "Greška - aktivnost nije obrisana!" })
        }

        let text = "";

        filtrirano.forEach(aktivnost => {
            const { naziv, tip, pocetak, kraj, dan } = aktivnost;

            if (naziv) {
                text += `${naziv},${tip},${pocetak},${kraj},${dan}\n`
            }
        })

        fs.writeFile('aktivnosti.txt', text, (err) => {
            if (err) throw err;

            res.json({ message: "Uspješno obrisana aktivnost!" })
        });
    })

    app.delete('/v1/predmet/:naziv', function (req, res) {

        const predmet = req.params.naziv;

        const sviPredmeti = dajSvePredmete();

        // Ako nista ne treba izbrisati
        if (!sviPredmeti.includes(predmet)) {
            return res.json({ message: "Greška - predmet nije obrisan!" })
        }

        let text = "";

        const filtrirano = sviPredmeti.filter(p => p !== predmet);

        filtrirano.forEach((p, index) => {
            text += ((index !== 0 ? "\n" : '') + p )
        });

        fs.writeFile('predmeti.txt', text, (err) => {
            if (err) throw err;

            res.json({ message: "Uspješno obrisan predmet!" })
        });
    })

    app.delete('/v1/all', function (req, res) {
        fs.truncate('aktivnosti.txt', err => {
            if (err) {
                return res.json({ message: "Greška - sadržaj datoteka nije moguće obrisati!" })
            }
        });

        fs.truncate('predmeti.txt', err => {
            if (err) {
                return res.json({ message: "Greška - sadržaj datoteka nije moguće obrisati!" })
            }
        });

        res.json({message: "Uspješno obrisan sadržaj datoteka!"})
    })
}