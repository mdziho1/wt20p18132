const baseUrl = "http://localhost:3000";

let grupe = [];

function onPageLoad() {
    $.ajax({ url: baseUrl + '/v2/grupa' }).then(g => {
        grupe = g;

        const grupeSelect = document.getElementById("grupe");

        grupe.forEach(grupa => {
            grupeSelect.options.add(new Option(grupa.naziv, grupa.id));
        })
    });
}

document.getElementById("unosForma").addEventListener('submit', e => {
    e.preventDefault();

    const forma = $("#unosForma").serializeArray();

    const grupaId = forma.find(polje => polje.name === "grupa").value;

    const studentiText = forma.find(polje => polje.name === "studenti").value;

    const studenti = parsirajStudente(studentiText)

    if (studenti.length > 0) {
        $.post({
            url: baseUrl + '/v2/dodajStudente',
            data: {
                studenti: studenti,
                grupaId: grupaId
            },
        }).then(r => {
            const poruke = r.poruke;

            prikaziPoruke(poruke);
        })
    }
})

function parsirajStudente(studentiText) {

    const studenti = [];

    const studentiSplit = studentiText.split('\n');

    studentiSplit.forEach(studentText => {

        if (studentText) {
            const split = studentText.split(',');

            if (split.length === 2) {
                const student = {
                    ime: split[0].trim(),
                    index: split[1].trim()
                }

                studenti.push(student);
            }
        }
    })

    return studenti;
}

function prikaziPoruke(poruke) {
    let text = "";
    poruke.forEach(poruka => text += (poruka + '\n'));

    document.getElementById('studenti').value = text;
}