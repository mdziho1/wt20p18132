const baseUrl = "http://localhost:3000";

let predmeti = [];
let aktivnosti = [];

function onPageLoad() {
    $.ajax({
        url: baseUrl + '/v2/predmet', success: function (result) {
            predmeti = result;
        }
    });

    $.ajax({
        url: baseUrl + '/v2/aktivnost', success: function (result) {
            aktivnosti = result;
        }
    });
}

function ispisiPoruku(poruka) {
    $('#server-message').html(poruka);
}

function kreirajAktivnost(aktivnost) {
    return $.post({
        url: baseUrl + '/v2/aktivnost',
        data: aktivnost,
    }).done((result) => {
        ispisiPoruku("Uspješno dodana aktivnost!");
        aktivnosti.push(result);
    }).fail((result) => {
        ispisiPoruku(result.responseJSON.message);
    })
}

function dodajNovuAktivnost() {

    const forma = $('#nova-aktivnost').serializeArray();

    const novaAktivnost = {};

    forma.forEach(polje => {
        novaAktivnost[polje.name] = polje.value;
    })

    const predmetPostoji = predmeti.findIndex(p => p.naziv === novaAktivnost.naziv) !== -1;

    if (!predmetPostoji) {

        // Dodaj novi predmet
        $.post({
            url: baseUrl + '/v2/predmet',
            data: { naziv: novaAktivnost.naziv },
        }).done((noviPredmet) => {

            // Ako je uspjelo dodavanje predmeta, dodaj aktivnost
            kreirajAktivnost(novaAktivnost).done(() => {

                // Ako je uspjelo dodavanje i predmeta i aktivnosti
                predmeti.push(noviPredmet);

            }).fail((result) => {
                // U slucaju da ne uspije kreiranje aktivnosti, izbrisi predmet
                $.ajax({
                    url: `/v2/predmet/${noviPredmet.id}`,
                    type: 'DELETE',
                    success: function () {
                        ispisiPoruku("Aktivnost nije validna, predmet obrisan!");
                    }
                });
            })

        }).fail((result) => {
            ispisiPoruku(result.responseJSON.message);
        })
    } else {
        kreirajAktivnost(novaAktivnost);
    }
}

$('#nova-aktivnost').on('submit', e => {
    e.preventDefault();

    dodajNovuAktivnost();
})

