// Models
const { DataTypes } = require('sequelize');

module.exports = function (sequelize) {

    const Predmet = sequelize.define('Predmet', {
        // Model attributes are defined here
        naziv: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
    });

    const Grupa = sequelize.define('Grupa', {
        // Model attributes are defined here
        naziv: {
            type: DataTypes.STRING,
            allowNull: false
        },
    }, {
        name: {
            singular: 'Grupa',
            plural: 'Grupe',
        }
    });

    const Aktivnost = sequelize.define('Aktivnost', {
        // Model attributes are defined here
        naziv: {
            type: DataTypes.STRING,
            allowNull: false
        },
        pocetak: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
        kraj: {
            type: DataTypes.FLOAT,
            allowNull: false
        },
    });

    const Dan = sequelize.define('Dan', {
        // Model attributes are defined here
        naziv: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
    });

    const Tip = sequelize.define('Tip', {
        // Model attributes are defined here
        naziv: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
    });

    const Student = sequelize.define('Student', {
        // Model attributes are defined here
        ime: {
            type: DataTypes.STRING,
            allowNull: false
        },
        index: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
    });

    // RELACIJE

    // Predmet 1-N Grupa
    Predmet.hasMany(Grupa, {
        foreignKey: {
            allowNull: false
        }
    });
    Grupa.belongsTo(Predmet);

    // Aktivnost N-1 Predmet
    Aktivnost.belongsTo(Predmet, {
        foreignKey: {
            allowNull: false
        }
    });
    Predmet.hasMany(Aktivnost);

    // Aktivnost N-0 Grupa
    Aktivnost.belongsTo(Grupa);
    Grupa.hasMany(Aktivnost);

    // Aktivnost N-1 Dan
    Aktivnost.belongsTo(Dan, {
        foreignKey: {
            allowNull: false
        }
    });
    Dan.hasMany(Aktivnost);

    // Aktivnost N-1 Tip
    Aktivnost.belongsTo(Tip, {
        foreignKey: {
            allowNull: false
        }
    });
    Tip.hasMany(Aktivnost);

    // Student N-M Grupa
    Student.belongsToMany(Grupa, { through: 'StudentGroups' });
    Grupa.belongsToMany(Student, { through: 'StudentGroups' });

    return {
        Aktivnost,
        Student,
        Predmet,
        Grupa,
        Tip,
        Dan
    }
}