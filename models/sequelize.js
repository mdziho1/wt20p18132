const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('wt2018132', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        freezeTableName: true
    }
});

try {
    sequelize.authenticate().then(r => {
        console.log('Veza na bazu je uspješno uspostavljena.');
    })

} catch (error) {
    console.error('Nije moguće povezati se s bazom podataka!', error);
}

module.exports = sequelize;