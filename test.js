const app = require("./server");
const fs = require('fs');
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

function jsonParse(json) {
    try {
        return JSON.parse(json.replaceAll('\\', ''));
    } catch (ex) {
        return json;
    }
}

function parsirajTest(test) {

    const splitTest = test.split(',');

    const operacija = splitTest[0];
    const ruta = splitTest[1];
    let ulaz = jsonParse(splitTest[2]);
    let izlaz = jsonParse(splitTest[3]);

    let krajUlazaIdx = 3;

    // Provjerimo posebno za ulaz
    if (ulaz !== null) {
        const nevalidan = ulaz === jsonParse(ulaz);

        if (nevalidan) {

            let krajUlazaBrojac = splitTest.slice(2).findIndex(dio => {
                const zadnjiChar = dio.charAt(dio.length - 1);
                return zadnjiChar === "]" || zadnjiChar === "}"
            })

            krajUlazaIdx += krajUlazaBrojac;

            ulaz = jsonParse(splitTest.slice(2, krajUlazaIdx).join(','))
        }
    }

    izlaz = jsonParse(splitTest.slice(krajUlazaIdx).join(','));

    return {
        operacija,
        ruta,
        ulaz,
        izlaz
    }
}


const testovi = [];

const data = fs.readFileSync("testniPodaci.txt", "utf-8");

const split = data.split('\n');

split.forEach(test => {
    if (test) {
        testovi.push(parsirajTest(test))
    }
})

describe("Testovi", () => {
    testovi.forEach(test => {
        if (test.operacija === "GET") {
            it(`GET ${test.ruta}`, (done) => {
                chai.request(app)
                    .get(test.ruta)
                    .end((err, res) => {
                        res.body.should.be.eql(test.izlaz);
                        done();
                    });
            });
        } else if (test.operacija === "POST") {
            it(`POST ${test.ruta}`, (done) => {
                chai.request(app)
                    .post(test.ruta)
                    .send(test.ulaz)
                    .end((err, res) => {
                        res.body.should.be.eql(test.izlaz);
                        done();
                    });
            });
        } else if (test.operacija === "DELETE") {
            it(`DELETE ${test.ruta}`, (done) => {
                chai.request(app)
                    .delete(test.ruta)
                    .end((err, res) => {
                        res.body.should.be.eql(test.izlaz);
                        done();
                    });
            });
        }
    })

    // Ocisti fajlove
    after(() => {
        fs.truncate('predmeti.txt', 0, () => {
        });
        fs.truncate('aktivnosti.txt', 0, () => {
        });
    })
})
